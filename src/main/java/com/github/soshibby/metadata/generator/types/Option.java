package com.github.soshibby.metadata.generator.types;

import com.google.gson.annotations.Expose;

public class Option {
	@Expose
	private String name;
	@Expose
	private String dataType;
	@Expose
	private Object defaultValue;

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public String getDataType() {
		return this.dataType;
	}
	
	public void setDefaultValue(Object value) {
		this.defaultValue = value;
	}
	
	public Object getDefaultValue() {
		return this.defaultValue;
	}
}
