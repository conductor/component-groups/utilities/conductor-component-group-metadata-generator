package com.github.soshibby.metadata.generator.types;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class Method {
	private String name;
	@Expose
	private Map<String, String> parameters = new HashMap<String, String>();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Map<String, String> getParameters() {
		return this.parameters;
	}
}
