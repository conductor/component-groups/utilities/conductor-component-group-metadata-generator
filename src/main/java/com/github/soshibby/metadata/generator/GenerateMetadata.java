package com.github.soshibby.metadata.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import com.github.soshibby.metadata.generator.types.ComponentGroup;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateMetadata {

	private static final Logger log = LoggerFactory.getLogger(GenerateMetadata.class);

	public static void main(String[] args) {
		try {
			Parser parser = new Parser();
			ComponentGroup componentGroup = parser.parse();

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(componentGroup);
			log.info(json);
			
			File outputFile = new File("build/libs/component.group.metadata");
			outputFile.getParentFile().mkdirs();
			
			PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
			writer.print(json);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
