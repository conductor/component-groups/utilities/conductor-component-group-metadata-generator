package com.github.soshibby.metadata.generator.types;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Property {
	private String name;
	@Expose
	private Object value;
	@Expose
	private String dataType;
	@Expose
	@SerializedName("enum")
	private String[] enums;
	@Expose
	private Boolean readOnly;

	public Property() {

	}

	public Property(String name, String dataType, Object value) {
		this.name = name;
		this.dataType = dataType;
		this.value = value;
	}

	public Property(String name, String dataType, Object value, boolean isReadOnly) {
		this.name = name;
		this.dataType = dataType;
		this.value = value;
		this.readOnly = isReadOnly;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public Object getValue() {
		return this.value;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public String getDataType() {
		return this.dataType;
	}
	
	public void setEnum(String[] enums) {
		this.enums = enums;
	}
	
	public String[] getEnum() {
		return this.enums;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
}
