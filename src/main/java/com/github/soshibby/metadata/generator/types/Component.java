package com.github.soshibby.metadata.generator.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class Component {
	@Expose
	private String name;
	@Expose
	private String type;
	@Expose
	private List<Option> options = new ArrayList<Option>();
	@Expose
	private Map<String, Property> properties = new HashMap<String, Property>();
	@Expose
	private Map<String, Method> methods = new HashMap<String, Method>();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return this.type;
	}
	
	public Map<String, Method> getMethods() {
		return methods;
	}
	
	public Map<String, Property> getProperties() {
		return this.properties;
	}
	
	public void setOptions(List<Option> options) {
		this.options = options;
	}
	
	public List<Option> getOptions() {
		return this.options;
	}
}	
