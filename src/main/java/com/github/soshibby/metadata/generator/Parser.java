package com.github.soshibby.metadata.generator;

import com.github.soshibby.metadata.generator.types.Component;
import com.github.soshibby.metadata.generator.types.*;
import com.github.soshibby.metadata.generator.types.Method;
import com.github.soshibby.metadata.generator.types.Property;
import com.github.soshibby.metadata.generator.utils.ResourceUtil;
import com.github.soshibby.metadata.generator.utils.StringUtil;
import com.google.gson.Gson;
import org.conductor.component.annotations.*;
import org.conductor.component.types.DataType;
import org.reflections.Reflections;

import java.io.FileNotFoundException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.reflections.ReflectionUtils.*;

public class Parser {
    private final String META_DATA_FILE = "component.group.metadata";

    public ComponentGroup parse() throws Exception {
        return getComponentGroup();
    }

    private ComponentGroup getComponentGroup() throws Exception {
        String json;
        try {
            json = new ResourceUtil().getResourceFile(META_DATA_FILE);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Couldn't find '" + META_DATA_FILE + "' file, make sure that you have the metadata file in the 'src/main/resources' folder in your project.");
        }

        ComponentGroup componentGroup = new Gson().fromJson(json, ComponentGroup.class);
        componentGroup.setComponents(getComponents());
        componentGroup.setOptions(getComponentGroupOptions());
        return componentGroup;
    }

    private List<Option> getComponentGroupOptions() throws Exception {
        Reflections reflections = new Reflections("");
        Set<Class<?>> clsBootstrappers = reflections.getTypesAnnotatedWith(ComponentGroupBootstrap.class);
        Class<?> clsBootstrap;

        if (clsBootstrappers.size() == 0) {
            return new ArrayList();
        } else if (clsBootstrappers.size() == 1) {
            clsBootstrap = (Class<?>) clsBootstrappers.toArray()[0];
        } else {
            throw new Exception("Your project contains multiple component group bootstrappers, you can only have one.");
        }

        ComponentGroupOptions optionsAnnotation = clsBootstrap.getAnnotation(ComponentGroupOptions.class);
        List<Option> options = new ArrayList();

        if (optionsAnnotation == null) {
            return options;
        }

        for (ComponentGroupOption optionAnnotation : optionsAnnotation.options()) {
            Option option = new Option();
            option.setName(optionAnnotation.name());
            option.setDataType(optionAnnotation.dataType());
            if (!optionAnnotation.defaultValue().equals("")) {
                option.setDefaultValue(convertStringToDataType(optionAnnotation.dataType(), optionAnnotation.defaultValue()));
            }
            options.add(option);
        }

        return options;
    }

    private List<Component> getComponents() throws Exception {
        Reflections reflections = new Reflections("");
        Set<Class<?>> clsComponents = reflections.getTypesAnnotatedWith(org.conductor.component.annotations.Component.class);
        List<Component> components = new ArrayList<Component>();

        for (Class<?> clsComponent : clsComponents) {
            Component component = new Component();
            component.setName(StringUtil.lowercaseFirstLetter(clsComponent.getSimpleName()));
            component.setType(getComponentType(clsComponent));
            component.setOptions(getComponentOptions(clsComponent));

            for (Method method : getMethods(clsComponent)) {
                component.getMethods().put(method.getName(), method);
            }

            for (Property property : getProperties(clsComponent)) {
                component.getProperties().put(property.getName(), property);
            }

            for (Property property : getReadOnlyProperties(clsComponent)) {
                component.getProperties().put(property.getName(), property);
            }

            components.add(component);
        }

        return components;
    }

    private String getComponentType(Class<?> cls) {
        org.conductor.component.annotations.Component componentAnnotation = cls.getAnnotation(org.conductor.component.annotations.Component.class);
        return componentAnnotation.type();
    }

    private List<Option> getComponentOptions(Class<?> cls) {
        ComponentOptions optionsAnnotation = cls.getAnnotation(ComponentOptions.class);
        List<Option> options = new ArrayList<Option>();

        if (optionsAnnotation == null) {
            return options;
        }

        for (ComponentOption optionAnnotation : optionsAnnotation.value()) {
            Option option = new Option();
            option.setName(optionAnnotation.name());
            option.setDataType(optionAnnotation.dataType());
            if (!optionAnnotation.defaultValue().equals("")) {
                option.setDefaultValue(convertStringToDataType(optionAnnotation.dataType(), optionAnnotation.defaultValue()));
            }
            options.add(option);
        }

        return options;
    }

    private List<Method> getMethods(Class<?> clsComponent) {
        Set<java.lang.reflect.Method> clsMethods = getAllMethods(clsComponent, withAnnotation(org.conductor.component.annotations.Method.class));
        List<Method> methods = new ArrayList<Method>();

        for (java.lang.reflect.Method clsMethod : clsMethods) {
            Method method = new Method();
            method.setName(clsMethod.getName());

            for (Parameter clsParameter : clsMethod.getParameters()) {
                String parameterName = clsParameter.getName();
                String parameterDataType = getDataType(clsParameter.getType());
                method.getParameters().put(parameterName, parameterDataType);
            }

            methods.add(method);
        }

        return methods;
    }


    private List<Property> getReadOnlyProperties(Class<?> cls) {
        // Get multiple 'ReadOnlyProperty' annotations.
        ReadOnlyProperties properties = cls.getAnnotation(ReadOnlyProperties.class);

        if (properties == null) {
            // Check if there exist one 'ReadOnlyProperty' annotation on the class.
            ReadOnlyProperty property = cls.getAnnotation(ReadOnlyProperty.class);

            if (property == null) {
                return new ArrayList();
            } else {
                return Arrays.asList(convertReadOnlyPropertyToProperty(property));
            }
        }

        return Arrays.stream(properties.value())
                .map(this::convertReadOnlyPropertyToProperty)
                .collect(Collectors.toList());
    }

    private Property convertReadOnlyPropertyToProperty(ReadOnlyProperty property) {
        String dataType = dataTypeToString(property.dataType());
        Object value = convertStringToDataType(dataType, property.defaultValue());
        return new Property(property.name(), dataType, value, true);
    }

    private List<Property> getProperties(Class<?> cls) {
        Set<java.lang.reflect.Method> clsMethods = getAllMethods(cls, withAnnotation(org.conductor.component.annotations.Property.class), withParametersCount(1), withPrefix("set"));
        List<Property> properties = new ArrayList<Property>();

        for (java.lang.reflect.Method clsMethod : clsMethods) {
            Property property = new Property();
            property.setName(getPropertyName(clsMethod.getName()));
            String dataType = getDataType(clsMethod.getParameterTypes()[0]);
            property.setDataType(dataType);
            String initialValue = clsMethod.getAnnotation(org.conductor.component.annotations.Property.class).initialValue();
            property.setValue(convertStringToDataType(dataType, initialValue));
            String[] enums = clsMethod.getAnnotation(org.conductor.component.annotations.Property.class).enums();

            if (enums.length == 0) {
                property.setEnum(null);
            } else {
                property.setEnum(enums);
                property.setDataType("enum");
            }

            properties.add(property);
        }

        return properties;
    }

    private Object convertStringToDataType(String dataType, String value) {
        switch (dataType) {
            case "string":
                return value;
            case "integer":
                return Integer.parseInt(value);
            case "double":
                return Double.parseDouble(value);
            case "float":
                return Float.parseFloat(value);
            case "boolean":
                return Boolean.parseBoolean(value);
            default:
                throw new RuntimeException("Invalid data type. Cannot convert to data type '" + dataType + "'");
        }
    }

    private String getDataType(Class<?> parameterType) {
        String parameterDataType = parameterType.getSimpleName();

        switch (parameterDataType) {
            case "String":
            case "Double":
            case "Boolean":
            case "Float":
            case "Integer":
                return StringUtil.lowercaseFirstLetter(parameterDataType);
            case "double":
            case "boolean":
            case "float":
                return parameterDataType;
            case "int":
                return "integer";
            default:
                throw new RuntimeException("Unknown data type '" + parameterDataType + "'.");
        }
    }

    private String dataTypeToString(DataType dataType) {
        switch (dataType) {
            case STRING:
                return "string";
            case DOUBLE:
                return "double";
            case BOOLEAN:
                return "boolean";
            case INTEGER:
                return "integer";
            default:
                throw new RuntimeException("Unknown data type '" + dataType + "'.");
        }
    }

    private String getPropertyName(String methodName) {
        String propertyName = methodName.substring(3);
        return StringUtil.lowercaseFirstLetter(propertyName);
    }
}
