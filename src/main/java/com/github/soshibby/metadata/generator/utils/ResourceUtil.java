package com.github.soshibby.metadata.generator.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Henrik on 30/11/2016.
 */
public class ResourceUtil {

    public String getResourceFile(String fileName) throws FileNotFoundException {
        StringBuilder result = new StringBuilder("");

        ClassLoader classLoader = getClass().getClassLoader();
        URL resourcePath = classLoader.getResource(fileName);

        if (resourcePath == null) {
            throw new FileNotFoundException("Couldn't find file '" + fileName + "'");
        }

        File file = new File(resourcePath.getFile());

        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            result.append(line);
        }

        scanner.close();

        return result.toString();
    }

}
