package com.github.soshibby.metadata.generator.types;

import java.util.List;

import com.google.gson.annotations.Expose;

public class ComponentGroup {
	@Expose
	private String name;
	@Expose
	private String description;
	@Expose
	private String version;
	@Expose
	private List<Option> options;
	@Expose 
	private List<Component> components;
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getVersion() {
		return this.version;
	}
	
	public void setOptions(List<Option> options) {
		this.options = options;
	}
	
	public List<Option> getOptions() {
		return this.options;
	}
	
	public void setComponents(List<Component> components) {
		this.components = components;
	}
	
	public List<Component> getComponents() {
		return this.components;
	}
}
